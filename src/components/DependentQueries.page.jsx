import { useQuery } from "react-query";
import axios from "axios";

const fetchUserByEmail = (email) => {
  return axios.get(`http://localhost:4000/users/${email}`);
};

const fetchCoursesById = (id) => {
  return axios.get(`http://localhost:4000/channels/${id}`);
};

const DependentQueriesPage = ({ email }) => {
  const { data: user } = useQuery(["user", email], () =>
    fetchUserByEmail(email)
  );
  const channelId = user?.data.channelId;
  const { data: courses } = useQuery(
    ["courses", channelId],
    () => fetchCoursesById(channelId),
    {
      enabled: !!channelId,
    }
  );
  console.log(courses);
  return (
    <div>
      <h2>Dependent Queries</h2>
      {courses.data.courses.map((course) => {
        return <div>{course}</div>;
      })}
    </div>
  );
};

export default DependentQueriesPage;
