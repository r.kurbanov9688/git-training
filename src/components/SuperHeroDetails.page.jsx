import { useParams } from "react-router-dom";
import { useSuperHeroData } from "../hooks/useSuperHeroData";

const SuperHeroDetailsPage = () => {
  const { heroId } = useParams();
  const { data } = useSuperHeroData(heroId);
  return (
    <div>
      <h2>Super Hero Page</h2>
      {data?.data.alterEgo} {data?.data.name}
    </div>
  );
};

export default SuperHeroDetailsPage;
