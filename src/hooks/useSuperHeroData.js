import { useQuery, useMutation, useQueryClient } from "react-query";
import axios from "axios";

const fetchData = ({ queryKey }) => {
  const id = queryKey[1];
  console.log(queryKey);
  return axios.get(`http://localhost:4000/superheroes/${id}`);
};

const addHero = (hero) => {
  return axios.post("http://localhost:4000/superheroes", hero);
};
export const useSuperHeroData = (heroId) => {
  return useQuery(["super-hero", heroId], fetchData);
};

export const useAddHeroData = () => {
  const queryClient = useQueryClient();
  return useMutation(addHero, {
    onSuccess: (data) => {
      //   queryClient.invalidateQueries("super-hero");
      queryClient.setQueryData("super-hero", (oldQueryData) => {
        return {
          ...oldQueryData,
          data: [...oldQueryData.data, data.data],
        };
      });
    },
  });
};
