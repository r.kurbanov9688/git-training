import { useQuery } from "react-query";
import axios from "axios";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useAddHeroData } from "../hooks/useSuperHeroData";

export const fetchHeroes = () => {
  return axios.get("http://localhost:4000/superheroes");
};

const RQSuperHeroesPage = () => {
  const { data, isLoading, isError, error, refetch } = useQuery(
    "super-hero",
    fetchHeroes
  );
  const { mutate } = useAddHeroData();
  const [name, setName] = useState("");
  const [alterEgo, setAlterEgo] = useState("");

  const onNameChange = (e) => {
    setName(e.target.value);
  };
  console.log("render");

  const onAlterEgoChange = (e) => {
    setAlterEgo(e.target.value);
  };
  const onHandleClick = () => {
    const heroData = { name, alterEgo };
    mutate(heroData);
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <h2>{error.message}</h2>;
  }
  return (
    <div>
      <h2>RQ Super Heroes Page</h2>
      <input onChange={onNameChange} value={name} type="text" />{" "}
      <input onChange={onAlterEgoChange} value={alterEgo} type="text" />
      {data?.data.map((hero) => {
        return (
          <div key={hero.id}>
            <Link to={`/rq-super-heroes/${hero.id}`}>{hero.name}</Link>
          </div>
        );
      })}
      <button onClick={refetch}>Fetch data</button>
      <button onClick={onHandleClick}>Add a Hero</button>
    </div>
  );
};

export default RQSuperHeroesPage;
