import "./App.css";
import SuperHeroesPage from "./components/SuperHeroes.page";
import RQSuperHeroesPage from "./components/RQSuperHeroes.page";
import Home from "./components/Home.page";
import Navigation from "./components/Navigation";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { ReactQueryDevtools } from "react-query/devtools";
import { QueryClientProvider, QueryClient } from "react-query";
import SuperHeroDetailsPage from "./components/SuperHeroDetails.page";
import ParallelQueriesPage from "./components/ParallelQueries.page";
import DependentQueriesPage from "./components/DependentQueries.page";

const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <div className="App">
          <Navigation />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/super-heroes" element={<SuperHeroesPage />} />
            <Route path="/rq-super-heroes" element={<RQSuperHeroesPage />} />
            <Route
              path="/rq-super-heroes/:heroId"
              element={<SuperHeroDetailsPage />}
            />
            <Route path="/parallel-queries" element={<ParallelQueriesPage />} />
            <Route
              path="/dependent-queries"
              element={<DependentQueriesPage email={"vishwas@example.com"} />}
            />
          </Routes>

        </div>
      </BrowserRouter>
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}

export default App;
