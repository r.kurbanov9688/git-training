const { default: axios } = require("axios");
const { useQuery } = require("react-query");
const { fetchHeroes } = require("./RQSuperHeroes.page");

const fetchFriends = () => {
  return axios.get("http://localhost:4000/friends");
};

const ParallelQueriesPage = () => {
  const { data: friends } = useQuery("friends", fetchFriends);
  const { data: heroesq } = useQuery("heroes", fetchHeroes);
  return (
    <div>
      <h2>Parallel Queries</h2>
      {friends?.data.map(friend => <div>{friend.name}</div>)}
      {heroes?.data.map(hero => <div>{hero.name}</div>)}
    </div>
  );
};

export default ParallelQueriesPage;
